const express = require('express')
const app= express()

const port = process.env.PORT || 3001

app.get('/NL', (req, res)=>{ 
    res.send('Hallo Wereld')
})
app.get('/HI', (req, res)=>{ 
    res.send('नमस्ते दुनिया')
})
app.get('/FR', (req, res)=>{ 
    res.send('Bonjour le monde')
})
app.get('/ES', (req, res)=>{ 
    res.send('Hola Mundo')
})
app.get('/IT', (req, res)=>{ 
    res.send('Ciao Mondo')
})
app.get('/CH', (req, res)=>{ 
    res.send('你好，世界')
})
app.get('/JP', (req, res)=>{ 
    res.send('こんにちは世界')
})
app.get('/AR', (req, res)=>{ 
    res.send('مرحبا بالعالم')
})
app.get('/EN', (req, res)=>{ 
    res.send('Hello world')
})

app.listen(port, ()=>{
    console.log('Serving as it should be on port', port)
})